package com.dlabs.fm.auth.external.drive;

import com.dlabs.fm.auth.core.dtos.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Service
@Slf4j
public class DriveService {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${drive.urlbase}")
    private String URL_BASE_DRIVE;

    /**
     * Check if the file id exist.
     * If fails throws exception that must been handled by error rest template handler.
     * @param fileId file
     */
    public void checkFileExistById(String fileId) {
        ResponseDto response = this.restTemplate.getForEntity(URL_BASE_DRIVE +"/files/existence/" + fileId, ResponseDto.class).getBody();
        log.info(Objects.requireNonNull(response).toString());
    }

}
