package com.dlabs.fm.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class AuthApplication {

	// Starting Point of the Application
	public static void main(String[] args) {
		SpringApplication.run(AuthApplication.class, args);
	}

}
