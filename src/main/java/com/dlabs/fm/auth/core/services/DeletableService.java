package com.dlabs.fm.auth.core.services;

import com.dlabs.fm.auth.core.repositories.DeletableRepository;
import com.dlabs.fm.auth.core.BusinessException;
import com.dlabs.fm.auth.core.models.BaseModel;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

@Transactional(rollbackFor = Exception.class)
public interface DeletableService<
        R extends DeletableRepository<E, ID>,
        E extends BaseModel<ID>,
        ID
        >
        extends BaseService<R, E, ID> {

    default void deleteById(ID id) {

        if (!this.getRepository().existsById(id)) {
            throw new BusinessException(String.format("Id %s not exist", id));
        }

        this.getRepository().deleteById(id, new Date(), "System");

    }

}
