package com.dlabs.fm.auth.core;

import com.dlabs.fm.auth.core.dtos.ResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Controller;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.servlet.NoHandlerFoundException;

import java.util.Map;
import java.util.Objects;

@ControllerAdvice
@Controller
@ResponseBody
@Slf4j
public class GlobalExceptionHandler {

    // Business Exception
    @ExceptionHandler({BusinessException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto businessExceptionHandler(BusinessException e, WebRequest request) {
        log.error(e.getMessage());
        return this.buildResponse(e.getMessage());
    }

    // Invalid Path Variable URL
    @ExceptionHandler({MethodArgumentTypeMismatchException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto methodArgumentTypeMismatchExceptionHandler(MethodArgumentTypeMismatchException e, WebRequest request) {
        log.error(e.getMessage());
        return this.buildResponse("Invalid URL.");
    }

    @ExceptionHandler({MissingServletRequestParameterException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto missingServletRequestParameterExceptionHandler(MissingServletRequestParameterException e, WebRequest request) {
        log.error(e.getMessage());
        return this.buildResponse("Invalid parameters.");
    }

    // Invalid Json Body
    @ExceptionHandler({HttpMediaTypeNotSupportedException.class, HttpMessageNotReadableException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto invalidJsonBodyExceptionHandler(Exception e, WebRequest request) {
        log.error(e.getMessage());
        return this.buildResponse("Invalid json body.");
    }

    // Invalid Json Parameter
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e, WebRequest request) {
        log.error(e.getMessage());
        return this.buildResponse(Objects.requireNonNull(e.getFieldError()).getDefaultMessage());
    }

    // URL Not Found
    @ExceptionHandler({NoHandlerFoundException.class, HttpRequestMethodNotSupportedException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseDto notFoundExceptionHandler(Exception e, WebRequest request) {
        log.error(e.getMessage());
        return this.buildResponse("Resource not found.");

    }

    // File too large
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseDto handleMaxSizeExceptionHandler(MaxUploadSizeExceededException e, WebRequest request) {
        log.error(e.getMessage());
        return this.buildResponse("File to large.");
    }

    // Internal Server Error
    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseDto allExceptionHandler(Exception e, WebRequest request) {
        e.printStackTrace();
        log.error("Internal Server Error {}.", e.getMessage());
        return this.buildResponse("Internal server error.");
    }

    private ResponseDto buildResponse(String message) {
        return ResponseDto.builder()
                .data(Map.of("message", message))
                .build();
    }

}
