package com.dlabs.fm.auth.core.services;

import com.dlabs.fm.auth.core.repositories.BaseRepository;
import com.dlabs.fm.auth.core.models.BaseModel;

public interface BaseService<
        R extends BaseRepository<E, I>,
        E extends BaseModel<I>,
        I
        > {

    R getRepository();

}
