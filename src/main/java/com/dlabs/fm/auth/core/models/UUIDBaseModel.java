package com.dlabs.fm.auth.core.models;

import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.util.UUID;

@MappedSuperclass
@SuperBuilder
@Data
@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded=true)
@ToString(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
public abstract class UUIDBaseModel extends BaseModel<UUID> {

    @Id
    @GeneratedValue
    @Column(columnDefinition = "uuid")
    @EqualsAndHashCode.Include
    protected UUID id;

}
