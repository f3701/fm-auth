package com.dlabs.fm.auth.core.dtos;

import com.dlabs.fm.auth.core.Mapper;
import lombok.*;

@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ResponseDto {

    private Object data;

    // Custom Setter for Builder
    public static class ResponseDtoBuilder {

        public ResponseDtoBuilder() {
        }

        public ResponseDtoBuilder data(Object data, Class dto) {
            this.data = Mapper.modelMapper.map(data, dto);
            return this;
        }

        public ResponseDtoBuilder data(Object data) {
            this.data = data;
            return this;
        }
    }

}
