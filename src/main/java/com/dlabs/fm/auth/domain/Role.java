package com.dlabs.fm.auth.domain;

import com.dlabs.fm.auth.core.models.UUIDBaseModel;
import lombok.*;
import javax.persistence.*;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "roles")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded=true, callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role extends UUIDBaseModel {

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToMany(fetch= FetchType.LAZY)
    @JoinTable(
            name = "roles_permissions",
            joinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"), // owner
            inverseJoinColumns = @JoinColumn(
                    name = "permission_id", referencedColumnName = "id"))
    private Collection<Permission> permissions;
}
