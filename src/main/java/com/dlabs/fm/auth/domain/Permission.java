package com.dlabs.fm.auth.domain;

import com.dlabs.fm.auth.core.models.UUIDBaseModel;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "permissions")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded=true, callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Permission extends UUIDBaseModel {

    @Column(name = "name", unique = true)
    private String name;

    @Column(name = "description")
    private String description;

    @OneToMany(fetch= FetchType.LAZY)
    @JoinColumn(name = "permission_id")
    private Collection<Endpoint> endpoints;

}
