package com.dlabs.fm.auth.domain;

import com.dlabs.fm.auth.core.models.UUIDBaseModel;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "endpoints")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded=true, callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Endpoint extends UUIDBaseModel {

    @Column(name = "value", unique = true)
    private String value;

    @Column(name = "method")
    private String method;

}
