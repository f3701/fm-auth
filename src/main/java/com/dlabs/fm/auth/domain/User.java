package com.dlabs.fm.auth.domain;

import com.dlabs.fm.auth.core.models.UUIDBaseModel;
import lombok.*;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "users")
@Data
@EqualsAndHashCode(onlyExplicitlyIncluded=true, callSuper = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User extends UUIDBaseModel {

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    @ManyToMany(fetch=FetchType.LAZY)
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"), // joinColumns: owner
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;

}
