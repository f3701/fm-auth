package com.dlabs.fm.auth.shared;

import com.dlabs.fm.auth.core.BusinessException;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class UUIDValidator {


    public void validateFormat(String uuidStr) {
        try {
            UUID.fromString(uuidStr);
        } catch (IllegalArgumentException e) {
            throw new BusinessException("Invalid UUID format. " + uuidStr);
        }
    }
}

