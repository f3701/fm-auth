CREATE TABLE auth.users
(
    -- Base
    id              uuid                  DEFAULT uuid_generate_v4() NOT NULL,

    -- Auditory
    created_by      VARCHAR(256) NOT NULL DEFAULT 'SYSTEM',
    created_at      TIMESTAMP    NOT NULL DEFAULT NOW(),

    updated_last_by VARCHAR(256)          DEFAULT NULL,
    updated_last_at TIMESTAMP             DEFAULT NULL,

    deleted_by      VARCHAR(256)          DEFAULT NULL,
    deleted_at      TIMESTAMP             DEFAULT NULL,

    -- Parametric
    username        VARCHAR               DEFAULT NULL,
    password        VARCHAR               DEFAULT NULL

);
