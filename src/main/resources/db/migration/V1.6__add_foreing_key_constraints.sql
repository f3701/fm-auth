ALTER TABLE auth.users_roles
    ADD CONSTRAINT fk_users_roles_table_role_id_to_roles
        FOREIGN KEY (role_id) REFERENCES roles (id),

    ADD CONSTRAINT fk_users_roles_table_user_id_to_users
        FOREIGN KEY (user_id) REFERENCES users (id)
;

ALTER TABLE auth.roles_permissions
    ADD CONSTRAINT fk_roles_permissions_table_role_id_to_roles
        FOREIGN KEY (role_id) REFERENCES roles (id),

    ADD CONSTRAINT fk_roles_permissions_table_permission_id_to_permissions
        FOREIGN KEY (permission_id) REFERENCES permissions (id)
;

ALTER TABLE auth.endpoints
    ADD CONSTRAINT fk_endpoints_table_permission_id_to_permissions
        FOREIGN KEY (permission_id) REFERENCES permissions (id)
;
