ALTER TABLE auth.users
    ADD CONSTRAINT PK_users PRIMARY KEY (id)
;

ALTER TABLE auth.users_roles
    ADD CONSTRAINT PK_users_roles PRIMARY KEY (id)
;

ALTER TABLE auth.roles
    ADD CONSTRAINT PK_roles PRIMARY KEY (id)
;

ALTER TABLE auth.roles_permissions
    ADD CONSTRAINT PK_roles_permissions PRIMARY KEY (id)
;

ALTER TABLE auth.permissions
    ADD CONSTRAINT PK_permissions PRIMARY KEY (id)
;

ALTER TABLE auth.endpoints
    ADD CONSTRAINT PK_endpoints PRIMARY KEY (id)
;