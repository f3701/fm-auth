CREATE TABLE auth.users_roles
(
    -- Base
    id              uuid                  DEFAULT uuid_generate_v4() NOT NULL,

    -- Auditory
    created_by      VARCHAR(256) NOT NULL DEFAULT 'SYSTEM',
    created_at      TIMESTAMP    NOT NULL DEFAULT NOW(),

    updated_last_by VARCHAR(256)          DEFAULT NULL,
    updated_last_at TIMESTAMP             DEFAULT NULL,

    deleted_by      VARCHAR(256)          DEFAULT NULL,
    deleted_at      TIMESTAMP             DEFAULT NULL,

    -- Parametric
    user_id         uuid                  DEFAULT NULL,
    role_id         uuid                  DEFAULT NULL

);

CREATE TABLE auth.roles_permissions
(
    -- Base
    id              uuid                  DEFAULT uuid_generate_v4() NOT NULL,

    -- Auditory
    created_by      VARCHAR(256) NOT NULL DEFAULT 'SYSTEM',
    created_at      TIMESTAMP    NOT NULL DEFAULT NOW(),

    updated_last_by VARCHAR(256)          DEFAULT NULL,
    updated_last_at TIMESTAMP             DEFAULT NULL,

    deleted_by      VARCHAR(256)          DEFAULT NULL,
    deleted_at      TIMESTAMP             DEFAULT NULL,

    -- Parametric
    role_id         uuid                  DEFAULT NULL,
    permission_id         uuid                  DEFAULT NULL

);
